package me.jordandi123.cooker.commands;

import me.jordandi123.cooker.Cooker;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * This class handles the cook command
 *
 * Copyright 2019-2020 (c) JordanDi123. All Rights Reserved.
 *
 * @author JordanDi123
 */
public class CookCommand implements CommandExecutor {

    private Cooker cooker;

    public CookCommand(Cooker cooker) {
        this.cooker = cooker;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(!(sender instanceof Player)) {
            sender.sendMessage("Only players may execute this command");
            return true;
        }

        return false;
    }
}
